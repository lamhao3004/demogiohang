var prodList = new ProductList();
var validation = new Validation();

const addProd = function () {
   const proId = document.getElementById("id").value;
   const prodName = document.getElementById("name").value;
   const prodImage = getEle("image").value;
   const prodDescription = document.getElementById("description").value;

   const prodPrice = document.getElementById("price").value;

   const prodInventory = document.getElementById("inventory").value;

   const prodRating = document.getElementById("rating").value;
   const prodType = document.getElementById("type").value;

   // validate
   var isValid = true;
   isValid &=
      validation.checkEmpty(proId, "mã sản phẩm không được rỗng", "sp-id") &&
      validation.checkLength(proId, "Do dai ky tu 1-4", "sp-id", 0, 5) &&
      validation.checkNumber(proId, "Ma phai la so!", "sp-id") &&
      validation.checkTrungMa(proId, "Ma da ton tai!", "sp-id", prodList.arr);
   isValid &= validation.checkEmpty(
      prodName,
      "tên sản phẩm không được rỗng",
      "sp-name"
   );
   isValid &= validation.checkEmpty(
      prodImage,
      "hình ảnh phẩm không được rỗng",
      "sp-file"
   );
   isValid &= validation.checkEmpty(
      prodDescription,
      "mô tả phẩm không được rỗng",
      "sp-des"
   );
   isValid &= validation.checkEmpty(
      prodPrice,
      "Giá sản phẩm không được rỗng",
      "sp-price"
   );

   isValid &= validation.checkEmpty(
      prodInventory,
      "số lượng còn lại không được rỗng",
      "sp-inventory"
   );

   isValid &= validation.checkPosition("type", "Phai chon loai!", "sp-type");
   if (!isValid) {
      return;
   }
   //2. tạo một đối tượng sản phẩm từ dữ liệu ngta nhập
   const newProd = new Product(
      proId,
      prodName,
      prodImage,
      prodDescription,
      prodPrice,
      prodInventory,
      prodRating,
      prodType
   );
   console.log(newProd);
   axios({
      method: "POST",
      url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
      data: newProd,
   })
      .then(function (res) {
         // console.log(res.data);
         fetchProd();
      })
      .catch(function (err) {
         console.log(err);
      });
};
const getProdById = function () {
   var id = document.getElementById("search").value;
   // console.log(findId);
   axios({
      method: "GET",
      url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/" + id,
   })
      .then(function (res) {
         var arr = [];
         arr.push(res.data);
         prodList.arr = arr;
         renderProd();
      })
      .catch(function (err) {
         console.log(err);
      });
};

const renderProd = function (list = prodList.arr) {
   var htmlContent = "";

   for (var i = 0; i < list.length; i++) {
      //template string
      htmlContent += 
   `<tr>
      <td>${i + 1}</td>
      <td>${list[i].id}</td>
      <td>${list[i].name}</td>
      <td> <img style="width: 100px;" src="${list[i].image}" ></td>
      <td>${list[i].description}</td>
      <td>${list[i].price}</td>
      <td>${list[i].inventory}</td>
      <td>${list[i].rating}</td>
      <td>${list[i].type}</td>
      <td>
        <button class="btn btn-info" onclick="editButton(${
           list[i].id
        })">Edit</button>

        <button class="btn btn-danger" onclick="deleteProduct(${
           list[i].id
        })">Delete</button>
      </td>
   </tr>`;
   // <button
   //        onclick="goToDeTail(${list[i].id})"
   //        class="btn btn-success" >
   //        View Detail
   //      </button>
   }
   document.getElementById("tbodyProd").innerHTML = htmlContent;
};


let updateProduct = function(){
   let id = getEle('id').value;
   let name = getEle('name').value;
   let image = getEle('image').value;
   let description = getEle('description').value;
   let price = getEle('price').value;
   let inventory = getEle('inventory').value;
   let rating = getEle('rating').value;
   let type = getEle('type').value;

   let product = new Product(id,name,image,description,price,inventory,rating,type);

   let resolver  = function(res){
      fetchProd();
   };
   let rejecter  = function(err){
      console.log(err)
   };
   axios({
      url: `https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/${id}`,
      method: 'PUT',
      data: product,
   })
   .then(resolver)
   .catch(rejecter)
};



let deleteProduct = function(id){
   let resolver  = function(res){
      console.log(res)
      fetchProd();
   }

   let rejecter  = function(err){
      console.log(err);
   }
   axios({
      url: `https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/${id}` ,
      method: 'DELETE',
   })
   .then(resolver)
   .catch(rejecter)
 }


let editButton = function(id){
   let product = prodList.getProductById(id);
   getEle('id').value = product.id;
   getEle('name').value = product.name;
   getEle('image').value = product.image;
   getEle('description').value = product.description;
   getEle('price').value = product.price;
   getEle('inventory').value = product.inventory;
   getEle('rating').value = product.rating;
   getEle('type').value = product.type;

   getEle('btnAdd').style.display = "none";
   getEle('btnUpdate').style.display = "block";
   getEle('btnCancle').style.display = "block";
   getEle('id').setAttribute("disabled",true);
}


let cancleButton = function(){
   getEle('btnAdd').style.display = "block";
   getEle('btnUpdate').style.display = "none";
   getEle('btnCancle').style.display = "none";
   getEle('formProduct').reset();
   getEle('id').removeAttribute("disabled");
}


function getEle(id) {
   return document.getElementById(id);
}


const fetchProd = function () {
   // Hàm xử lý khi lấy dữ liệu thành công
   const resolver = function (res) {
      // console.log(res);
      prodList.arr = res.data;
      renderProd();
   };
   // Hàm xử lý khi lấy dữ liệu thất bại
   const rejecter = function (err) {
      console.log(err);
   };
   axios({
      url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
      method: "GET",
   })
      //  callBack function
      .then(resolver)
      .catch(rejecter);
};

function getEle(id) {
   return document.getElementById(id);
}
getEle("search").addEventListener("keyup", function () {
   var id = document.getElementById("search").value;

   if (id === "") {
      fetchProd();
      // alert("Khong tim thay san pham");
   } else {
      getProdById();
   }
});
fetchProd();
