const ProductList = function () {
   this.arr = [];
   this.addProduct = function (prod) {
      this.arr.push(prod);
   };

   this.getProductById = function(Id){
      var result = this.arr.find(function(pro){
         return parseInt(pro.id) === Id;
      });
      return result;
   };
};
